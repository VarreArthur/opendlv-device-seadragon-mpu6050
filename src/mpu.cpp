/*
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, version 3.
 *
 * This program is distributed in the hope that it will be useful, but
 * WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
 * General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program. If not, see <http://www.gnu.org/licenses/>.
 */

#include <linux/i2c-dev.h>
#include <sys/ioctl.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <stdint.h>
#include <iostream>
#include <stdexcept>
#include <string>
#include <tuple>
#include <cluon-complete.hpp>

// all the adress comes from the MPU6050 register map 

#define MPU6050_I2C_ADDR 0x68


// define the accel adress 
#define REG_ACCEL_XOUT_H 0x3B
#define REG_ACCEL_XOUT_L 0x3C
#define REG_ACCEL_YOUT_H 0x3D
#define REG_ACCEL_YOUT_L 0x3E
#define REG_ACCEL_ZOUT_H 0x3F
#define REG_ACCEL_ZOUT_L 0x40

// define the gyro adress
#define REG_GYRO_XOUT_H 0x43
#define REG_GYRO_XOUT_L 0X44
#define REG_GYRO_YOUT_H 0x45
#define REG_GYRO_YOUT_L 0x46
#define REG_GYRO_ZOUT_H 0x47
#define REG_GYRO_ZOUT_L 0x48


#define REG_PWR_MGMT_1 0x6B
#define REG_ACCEL_CONFIG 0x1C
#define REG_GYRO_CONFIG 0x1B

int file = -1;

// Please note, this is not the recommanded way to write data
// to i2c devices from user space.
void i2c_write(__u8 reg_address, __u8 val) {
	char buf[2];
	if(file < 0) {
		printf("Error, i2c bus is not available\n");
		exit(1);
	}

	buf[0] = reg_address;
	buf[1] = val;

	if (write(file, buf, 2) != 2) {
		printf("Error, unable to write to i2c device\n");
		exit(1);
	}

}

// Please note, this is not thre recommanded way to read data
// from i2c devices from user space.
char i2c_read(uint8_t reg_address) {
	char buf[1];
	if(file < 0) {
		printf("Error, i2c bus is not available\n");
		exit(1);
	}

	buf[0] = reg_address;

	if (write(file, buf, 1) != 1) {
		printf("Error, unable to write to i2c device\n");
		exit(1);
	}


	if (read(file, buf, 1) != 1) {
		printf("Error, unable to read from i2c device\n");
		exit(1);
	}

	return buf[0];

}

uint16_t merge_bytes( uint8_t LSB, uint8_t MSB) {
	return  (uint16_t) ((( LSB & 0xFF) << 8) | MSB);
}


// 16 bits data on the MPU6050 are in two registers,
// encoded in two complement. So we convert those to int16_t
int16_t two_complement_to_int( uint8_t LSB, uint8_t MSB) {
	int16_t signed_int = 0;
	uint16_t word;

	word = merge_bytes(LSB, MSB);

	if((word & 0x8000) == 0x8000) { // negative number
		signed_int = (int16_t) -(~word);
	} else {
		signed_int = (int16_t) (word & 0x7fff);
	}

	return signed_int;
}

std::tuple<float, float, float> GetAccelData ( int i2c_bus =1){
	char bus_filename[250];
	char accel_z_h , accel_z_l, accel_x_h, accel_x_l, accel_y_h, accel_y_l;
	int16_t x_accel = 0;
	int16_t y_accel = 0;
	int16_t z_accel = 0;
	float x_accel_g, y_accel_g,z_accel_g;


// Reach the i2c port 
	snprintf(bus_filename, 250,"/dev/i2c-%i", i2c_bus);
	file = open(bus_filename, O_RDWR);
	if (file < 0) {
		/* ERROR HANDLING; you can check errno to see what went wrong */
		exit(1);
	}
	if (ioctl(file, I2C_SLAVE, MPU6050_I2C_ADDR) < 0) {
		/* ERROR HANDLING; you can check errno to see what went wrong */
		exit(1);
	}

// wake up the IMU 
	i2c_write(REG_PWR_MGMT_1, 0x01);
// config the acceleration register to scale range of 2g 
	i2c_write(REG_ACCEL_CONFIG, 0x00);

// Read the x accel data
	accel_x_h = i2c_read(REG_ACCEL_XOUT_H);
	accel_x_l = i2c_read(REG_ACCEL_XOUT_L);
// Read the y accel data 
	accel_y_h = i2c_read(REG_ACCEL_YOUT_H);
	accel_y_l = i2c_read(REG_ACCEL_YOUT_L);
// Read the z accel data 
	accel_z_h = i2c_read(REG_ACCEL_ZOUT_H);
	accel_z_l = i2c_read(REG_ACCEL_ZOUT_L);

// convert the accel data 
	x_accel= two_complement_to_int(accel_x_h,accel_x_l);
	x_accel_g = ((float) x_accel)/16384;

	y_accel= two_complement_to_int(accel_y_h,accel_y_l);
	y_accel_g = ((float) y_accel)/16384;

	z_accel= two_complement_to_int(accel_z_h,accel_z_l);
	z_accel_g = ((float) z_accel)/16384;

	return {x_accel_g,y_accel_g,z_accel_g};
}

std::tuple<float, float, float> GetGyroData ( int i2c_bus =1){
	char bus_filename[250];
	char gyro_z_h , gyro_z_l, gyro_x_h, gyro_x_l, gyro_y_h, gyro_y_l;
	int16_t x_gyro = 0;
	int16_t y_gyro = 0;
	int16_t z_gyro = 0;
	float x_gyro_deg, y_gyro_deg,z_gyro_deg;


// Reach the i2c port 
	snprintf(bus_filename, 250,"/dev/i2c-%i", i2c_bus);
	file = open(bus_filename, O_RDWR);
	if (file < 0) {
		/* ERROR HANDLING; you can check errno to see what went wrong */
		exit(1);
	}
	if (ioctl(file, I2C_SLAVE, MPU6050_I2C_ADDR) < 0) {
		/* ERROR HANDLING; you can check errno to see what went wrong */
		exit(1);
	}

// wake up the IMU 
	i2c_write(REG_PWR_MGMT_1, 0x01);
// config the gyro register to scale range of 250°
	i2c_write(REG_GYRO_CONFIG, 0x00);

// Read the x gyro data
	gyro_x_h = i2c_read(REG_GYRO_XOUT_H);
	gyro_x_l = i2c_read(REG_GYRO_XOUT_L);
// Read y gyro data
	gyro_y_h = i2c_read(REG_GYRO_YOUT_H);
	gyro_y_l = i2c_read(REG_GYRO_YOUT_L);
// Read z gyro data
	gyro_z_h = i2c_read(REG_GYRO_ZOUT_H);
	gyro_z_l = i2c_read(REG_GYRO_ZOUT_L);

// Convert the gyro data 
	x_gyro= two_complement_to_int(gyro_x_h,gyro_x_l);
	x_gyro_deg = ((float) x_gyro)/131;
	y_gyro= two_complement_to_int(gyro_y_h,gyro_y_l);
	y_gyro_deg = ((float) y_gyro)/131;
	z_gyro= two_complement_to_int(gyro_z_h,gyro_z_l);
	z_gyro_deg = ((float) z_gyro)/131;

	return {x_gyro_deg,y_gyro_deg,z_gyro_deg};
}

int main(){
	while (true){
		sleep(1);
		const auto AccelData=GetAccelData();
		const auto GyroData=GetGyroData();
		std::cout <<" | accel : " 
		<< std::get<0>(AccelData)<< ","
		<< std::get<1>(AccelData)<< ","
		<< std::get<2>(AccelData)<< "| \t"
		<< " gyro : " 
		<< std::get<0>(GyroData)<< ","
		<< std::get<1>(GyroData)<< ","
		<< std::get<2>(GyroData)<< "| \n";
	}
	
}