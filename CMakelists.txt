cmake_minimum_required(VERSION 3.2)
project(mpu)
set(CMAKE_CXX_STANDARD 17)
set(CMAKE_CXX_FLAGS "${CMAKE_CXX_FLAGS} -Wall -Wextra")

add_executable(${PROJECT_NAME} ${CMAKE_CURRENT_SOURCE_DIR}/mpu.cpp
